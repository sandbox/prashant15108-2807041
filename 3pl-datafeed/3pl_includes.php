<?php
class GForm {
	var $extLoginData  = array (
		'ThreePLKey' => "a5064a1f-f234-4aab-bd28-5a8d882a5ef8",
		'Login'      => "kaefer",
		'Password'   => "kaefer",
		'FacilityID' => 1,
		'ThreePLID'  => 605
	);
	var $loginData     = array (
		'ThreePLID'  => 605,
		'Login'      => "kaefer",
		'Password'   => "kaefer"
	);
	public $client        = null;
	public $soapUrl       = 'http://www.kaefer-nostalgie.com/sites/all/modules/custom_warehouse/3pl-datafeed/XML_Orders_WSDL.wsdl'; //This is a url to the endpoint, so you may have to hardcode it with a full http address
	public $options       = array(); 
	function __construct() {
		$this->client = new SoapClient($this->soapUrl, $this->options);
	}
	function CreateOrders( $ExternalLoginData, $ArrayOfOrder, $warnings ){
		try {
			$funcRet = $this->client->CreateOrders( $ExternalLoginData, $ArrayOfOrder, $warnings );
		} catch ( Exception $e ) {
			print '(CreateOrdersResult) SOAP Error: - ' . $e->getMessage();
			exit;
		}
		return $funcRet; 
	}
	function ReportStockStatus( $userLoginData ){
		try {
			$funcRet = $this->client->ReportStockStatus( $userLoginData );
		} catch ( Exception $e ) {
			print '(ReportStockStatus) SOAP Error: - ' . $e->getMessage ();
			exit;
		}
		return $funcRet; 
	}
}


//If you want to use inventory updating, this function will grab all inventory values from 3PL and puts them in an $inventory array
//You can loop through this array and update the values in your system
function update_inventory_from_3pl() {
	$G_Form = new GForm;
	$item_inventory    = $G_Form->ReportStockStatus($G_Form->loginData);
	$item_inventory    = new SimpleXMLElement($item_inventory);
	$inventory         = array();	

	// Iterate over each item/location and create an array.
	// Because each item can be in multi locations, need to check for duplicate SKU's and add prev value
	foreach ($item_inventory->Q as $item) {
		$sku = (string)$item->SKU;
		if (!isset($inventory[$sku])) $inventory[$sku] = 0;
		$inventory[$sku] += (double)$item->SUMOFAVAILABLE;
	}

	// Iterate over our new array of SKU/Qty pairs and update the inventory count
	foreach ($inventory as $k => $v) {
		
		//Write your code here
		//$k is the SKU
		//$v is the new inventory value








	}
}

?>