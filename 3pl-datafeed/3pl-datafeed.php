<?php
/*
Written by David Hollander (david@sparkweb.net) with help from Jacob Dubail
2012-03-21
v1.0, For FoxyCart 0.7.2
*/


$api_key = "https://example.foxycart.com/api";

//Get Transaction Data Post From FoxyCart
if (isset($_POST["FoxyData"])) {
    	$FoxyData_received = $_POST["FoxyData"];
    	$FoxyData_encrypted = $FoxyData_received;
	$FoxyData_decrypted = rc4crypt::decrypt($api_key,urldecode($FoxyData_encrypted));

//Nothing to see here... move along
} else {
	die('No Content Received');
}


//Set 3PL Data
require_once "3pl_includes.php";


//Import FoxyData Response and Parse with SimpleXML
$xml = simplexml_load_string($FoxyData_decrypted, NULL, LIBXML_NOCDATA);


//For Each Transaction
foreach($xml->transactions->transaction as $transaction) {

	//Initialize 3PL
	$OrderLineItem = array();

	//This variable will tell us whether this is a multi-ship store or not
	$is_multiship = 0;

	//Get FoxyCart Customer Information
	$transaction_id =		(string)$transaction->id;
	$transaction_date =		(string)$transaction->transaction_date;
	$processor_response =	(string)$transaction->processor_response;
	$customer_ip =			(string)$transaction->customer_ip;
	$customer_id =			(string)$transaction->customer_id;
	$customer_first_name =	(string)$transaction->customer_first_name;
	$customer_last_name =	(string)$transaction->customer_last_name;
	$customer_company =		(string)$transaction->customer_company;
	$customer_email =		(string)$transaction->customer_email;
	$customer_password =	(string)$transaction->customer_password;
	$customer_address1 =	(string)$transaction->customer_address1;
	$customer_address2 =	(string)$transaction->customer_address2;
	$customer_city =		(string)$transaction->customer_city;
	$customer_state =		(string)$transaction->customer_state;
	$customer_postal_code =	(string)$transaction->customer_postal_code;
	$customer_country =		(string)$transaction->customer_country;
	$customer_phone =		(string)$transaction->customer_phone;


	//This is for a multi-ship store. The shipping addresses will go in a $shipto array with the address name as the key
	$shipto = array();
	foreach($transaction->shipto_addresses->shipto_address as $shipto_address) {
		$is_multiship = 1;
		$shipto_name = (string)$shipto_address->address_name;
		$shipto[$shipto_name] = array(
			'first_name' => (string)$shipto_address->shipto_first_name,
			'last_name' => (string)$shipto_address->shipto_last_name,
			'company' => (string)$shipto_address->shipto_company,
			'address1' => (string)$shipto_address->shipto_address1,
			'address2' => (string)$shipto_address->shipto_address2,
			'city' => (string)$shipto_address->shipto_city,
			'state' => (string)$shipto_address->shipto_state,
			'postal_code' => (string)$shipto_address->shipto_postal_code,
			'country' => (string)$shipto_address->shipto_country,
			'shipping_service_description' => (string)$shipto_address->shipto_shipping_service_description,
			'subtotal' => (string)$shipto_address->shipto_subtotal,
			'tax_total' => (string)$shipto_address->shipto_tax_total,
			'shipping_total' => (string)$shipto_address->shipto_shipping_total,
			'total' => (string)$shipto_address->shipto_total,
		);
	}

	//This is setup for a single ship store
	if (!$is_multiship) {
		$shipping_first_name =	(string)$transaction->shipping_first_name ? (string)$transaction->shipping_first_name : $customer_first_name;
		$shipping_last_name =	(string)$transaction->shipping_last_name ? (string)$transaction->shipping_last_name : $customer_last_name;
		$shipping_company =		(string)$transaction->shipping_company ? (string)$transaction->shipping_company : $customer_company;
		$shipping_address1 =	(string)$transaction->shipping_address1 ? (string)$transaction->shipping_address1 : $customer_address1;
		$shipping_address2 =	(string)$transaction->shipping_address2 ? (string)$transaction->shipping_address2 : $customer_address2;
		$shipping_city =		(string)$transaction->shipping_city ? (string)$transaction->shipping_city : $customer_city;
		$shipping_state =		(string)$transaction->shipping_state ? (string)$transaction->shipping_state : $customer_state;
		$shipping_postal_code =	(string)$transaction->shipping_postal_code ? (string)$transaction->shipping_postal_code : $customer_postal_code;
		$shipping_country =		(string)$transaction->shipping_country ? (string)$transaction->shipping_country : $customer_country;
		$shipping_phone =		(string)$transaction->shipping_phone ? (string)$transaction->shipping_phone : $customer_phone;
		$shipto_shipping_service_description = (string)$transaction->shipto_shipping_service_description;

		$shipto['DEFAULT'] = array(
			'first_name' => $shipping_first_name,
			'last_name' => $shipping_last_name,
			'company' => $shipping_company,
			'address1' => $shipping_address1,
			'address2' => $shipping_address2,
			'city' => $shipping_city,
			'state' => $shipping_state,
			'postal_code' => $shipping_postal_code,
			'country' => $shipping_country,
			'shipping_service_description' => $shipto_shipping_service_description,
		);

	}




	//For Each Transaction Detail
	$i = 0;
	foreach($transaction->transaction_details->transaction_detail as $transactiondetails) {
		$product_name = (string)$transactiondetails->product_name;
		$product_code = (string)$transactiondetails->product_code;
		$product_quantity = (int)$transactiondetails->product_quantity;
		$sub_token_url = (string)$transactiondetails->sub_token_url;
		$product_shipto = (string)$transactiondetails->shipto;
		$product_weight = (string)$transactiondetails->product_weight;
		if (!$product_shipto) $product_shipto = "DEFAULT";

		//3PL Create Item
		if (!array_key_exists($product_shipto, $OrderLineItem)) $OrderLineItem[$product_shipto] = array();
		$OrderLineItem[$product_shipto]['OrderLineItem'][$i] = array(
			'SKU'            => $product_code,
			'Qty'            => $product_quantity,
			'Packed'         => $product_weight,
			'CuFtPerCarton'  => '0.006' //You may want to adjust this value
		);
		$i++;
	}


	//Submit 3PL (for each ship to address or for single)
	foreach($shipto as $key => $shipto_address) {

		$date = date(DATE_ATOM);
		$country = $shipto_address['country'] == "US" ? "USA" : $shipto_address['country'];

		$ship = $shipto_address['shipping_service_description'];
		
		//Put any shipping rename rules here to match what 3PL expects
		$ship = str_replace("Home Delivery", "Ground Home", $ship);
		
		preg_match_all('/([^\s]+)/i', $ship, $shipping);
		$carrier = $shipping[0][0];
		array_shift($shipping[0]);
		$mode = implode(" ", $shipping[0]);	

		//Build Array
		$ArrayOfOrder  = array(
			'Order' => array(
				'TransInfo'            => array(
					'ReferenceNum'      => $transaction_id,
					'PONum'             => $transaction_id,
					'EarliestShipDate'  => $date,
					'ShipCancelDate'    => strtotime(date(DATE_ATOM, strtotime($date)) . " +1 month")
				),
				'ShipTo'               => array(
					'Name'          => $shipto_address['first_name'] . ' ' . $shipto_address['last_name'],
					'CompanyName'   => ($shipto_address['company'] ? $shipto_address['company'] : $shipto_address['first_name'] . ' ' . $shipto_address['last_name']),
					'Address'       => array(
						'Address1' => $shipto_address['address1'],
						'Address2' => $shipto_address['address2'],
						'City'     => $shipto_address['city'],
						'State'    => $shipto_address['state'],
						'Zip'      => $shipto_address['postal_code'],
						'Country'  => $country
					),
				'EmailAddress1' => $customer_email,
				'CustomerName'  => $shipto_address['first_name'] . ' ' . $shipto_address['last_name']
				),
				'ShippingInstructions' => array(
					'Carrier'      => $carrier,
					'Mode'         => $mode,
					'BillingCode'  => 'Prepaid'
				),
				'Notes'                => $processor_response,
				'PalletCount'          => 1,
				'OrderLineItems'       => $OrderLineItem[$key]
			)
		);

		//For testing, uncomment these two lines to see what's being generated
		//print_r($ArrayOfOrder);
		//die;

		//Create Object, 
		$G_Form = new GForm;
		$results = $G_Form->CreateOrders($G_Form->extLoginData, $ArrayOfOrder, '');
		
		//Display error, or ignore array with success information
		if (!is_array($results)) echo $results;


	} //End For Each Address

} //End For Each Transaction


//Update Inventory (if you want to update your local inventory after order is done)
//update_inventory_from_3pl();

//Done
die("foxy");









// ======================================================================================
// RC4 ENCRYPTION CLASS
// Do not modify.
// ======================================================================================
/**
 * RC4Crypt 3.2
 *
 * RC4Crypt is a petite library that allows you to use RC4
 * encryption easily in PHP. It's OO and can produce outputs
 * in binary and hex.
 *
 * (C) Copyright 2006 Mukul Sabharwal [http://mjsabby.com]
 *     All Rights Reserved
 *
 * @link http://rc4crypt.devhome.org
 * @author Mukul Sabharwal <mjsabby@gmail.com>
 * @version $Id: class.rc4crypt.php,v 3.2 2006/03/10 05:47:24 mukul Exp $
 * @copyright Copyright &copy; 2006 Mukul Sabharwal
 * @license http://www.gnu.org/copyleft/gpl.html
 * @package RC4Crypt
 */
 
/**
 * RC4 Class
 * @package RC4Crypt
 */
class rc4crypt {
	/**
	 * The symmetric encryption function
	 *
	 * @param string $pwd Key to encrypt with (can be binary of hex)
	 * @param string $data Content to be encrypted
	 * @param bool $ispwdHex Key passed is in hexadecimal or not
	 * @access public
	 * @return string
	 */
	function encrypt ($pwd, $data, $ispwdHex = 0)
	{
		if ($ispwdHex)
			$pwd = @pack('H*', $pwd); // valid input, please!
 
		$key[] = '';
		$box[] = '';
		$cipher = '';
 
		$pwd_length = strlen($pwd);
		$data_length = strlen($data);
 
		for ($i = 0; $i < 256; $i++)
		{
			$key[$i] = ord($pwd[$i % $pwd_length]);
			$box[$i] = $i;
		}
		for ($j = $i = 0; $i < 256; $i++)
		{
			$j = ($j + $box[$i] + $key[$i]) % 256;
			$tmp = $box[$i];
			$box[$i] = $box[$j];
			$box[$j] = $tmp;
		}
		for ($a = $j = $i = 0; $i < $data_length; $i++)
		{
			$a = ($a + 1) % 256;
			$j = ($j + $box[$a]) % 256;
			$tmp = $box[$a];
			$box[$a] = $box[$j];
			$box[$j] = $tmp;
			$k = $box[(($box[$a] + $box[$j]) % 256)];
			$cipher .= chr(ord($data[$i]) ^ $k);
		}
		return $cipher;
	}
	/**
	 * Decryption, recall encryption
	 *
	 * @param string $pwd Key to decrypt with (can be binary of hex)
	 * @param string $data Content to be decrypted
	 * @param bool $ispwdHex Key passed is in hexadecimal or not
	 * @access public
	 * @return string
	 */
	function decrypt ($pwd, $data, $ispwdHex = 0)
	{
		return rc4crypt::encrypt($pwd, $data, $ispwdHex);
	}
}

?>